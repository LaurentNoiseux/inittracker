﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogController : MonoBehaviour {

    private List<string> logText;
    public Text textUi;
	// Use this for initialization
	void Start () {
        logText = new List<string>();
        textUi.text = "";
    }
	
	// Update is called once per frame
	void Update () {
    }

    private void refreshLog()
    {
        textUi.text = "";
        foreach(string line in logText)
        {
            textUi.text = textUi.text + line;
        }
    }



    public void AddtoLog(string actionText)
    {
        if (logText.Count <= 44)
        {
            logText.Add(actionText);
            refreshLog();
        }
        else
        {
            logText.RemoveAt(0);
            logText.Add(actionText);
            refreshLog();
        }
    }
}
