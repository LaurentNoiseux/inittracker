﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class Controller : MonoBehaviour {

    public LogController logController;
    public IOScript ioControl;
    public Button buttonRead, buttonWrite, buttonCancelRead, buttonCancelWrite;
    public Button buttonNext, buttonPrevious;
    public Button buttonNew, buttonClear, buttonPanelAddConfirm,
        buttonAction, buttonDamage, buttonHealing, buttonConfirmAction,
        buttonAddCancel, buttonActionCancel, buttonUndo, buttonLoadAdventurer;
    public InputField inputName, inputInit, inputHp, inputDamage;
    public Canvas canvas1;
    public Dropdown dropdown1;
    public Text firstText, secondText, thirdText, fourthText, fifthText, sixthText, 
        seventhText, eighthText, ninthText, tenthText, eleventhText, twelfthText,
        thirtheenthText, fourtheenthText, fiftheenthText, sixtheenthText, seventheenthText,
        eighteenthText, nintheenthText, twentiethText;
    public GameObject panelAddCreatures, panelAction, panelLoadListe, panelWriteListe;

    private int highestInit, lowestInit, playingCreaturePosition = 0;
    public List<Creature> creatureArray, creatureArrayUndo;
    private List<Text> textArray;

    // Use this for initialization
    void Start() {
		buttonPrevious.onClick.AddListener(PreviousCreature);
        buttonNext.onClick.AddListener(NextCreature);
        buttonPanelAddConfirm.onClick.AddListener(NewCreatureListener);
        buttonClear.onClick.AddListener(ButtonClear);
        buttonNew.onClick.AddListener(OpenPanelAdd);
        buttonAction.onClick.AddListener(OpenPanelAction);
        buttonDamage.onClick.AddListener(DamageSelected);
        buttonHealing.onClick.AddListener(HealingSelected);
        buttonConfirmAction.onClick.AddListener(ConfirmAction);
        buttonAddCancel.onClick.AddListener(ClosePanelAdd);
        buttonActionCancel.onClick.AddListener(ClosePanelAction);
        buttonRead.onClick.AddListener(OpenPanelLoad);
        buttonCancelRead.onClick.AddListener(ClosePanelLoad);
        buttonWrite.onClick.AddListener(OpenPanelWrite);
        buttonCancelWrite.onClick.AddListener(ClosePanelWrite);
        buttonUndo.onClick.AddListener(Undo);
        buttonLoadAdventurer.onClick.AddListener(LoadAdventurer);


        creatureArray = new List<Creature>();
        textArray = new List<Text>();
        dropdown1.ClearOptions();

        textArray.Add(firstText);
        textArray.Add(secondText);
        textArray.Add(thirdText);
        textArray.Add(fourthText);
        textArray.Add(fifthText);
        textArray.Add(sixthText);
        textArray.Add(seventhText);
        textArray.Add(eighthText);
        textArray.Add(ninthText);
        textArray.Add(tenthText);
        textArray.Add(eleventhText);
        textArray.Add(twelfthText);
        textArray.Add(thirtheenthText);
        textArray.Add(fourtheenthText);
        textArray.Add(fiftheenthText);
        textArray.Add(sixtheenthText);
        textArray.Add(seventheenthText);
        textArray.Add(eighteenthText);
        textArray.Add(nintheenthText);
        textArray.Add(twentiethText);
        highestInit = 0;
        lowestInit = 20;
        ClearText(true);
        panelAddCreatures.SetActive(false);
        panelAction.SetActive(false);
        panelLoadListe.SetActive(false);
        panelWriteListe.SetActive(false);
        Screen.SetResolution(1200, 800, false);
    }
    void Update()
    {
        if (creatureArray.Count >= 20)
        {
            buttonNew.interactable = false;
        }
        else
        {
            buttonNew.interactable = true;
        }
    }
    private void LoadAdventurer()
    {
        Adventurer aventurier1 = new Adventurer("Idrianna", "Fighter", 20, 3, 10);
        Adventurer aventurier2 = new Adventurer("Orden", "Priest", 20, 3, 10);
        Adventurer aventurier3 = new Adventurer("Iorek", "Barbarian", 20, 3, 10);
        Adventurer aventurier4 = new Adventurer("Alak", "Rogue", 20, 3, 10);

        SortCreature(aventurier1);
        SortCreature(aventurier2);
        SortCreature(aventurier3);
        SortCreature(aventurier4);

    }
    // Undo Methods
    private void Undo()
    {
        creatureArray = new List<Creature>(creatureArrayUndo);
        PrintCreatures();
    }

    ///
    private void UndoPrep()
    {
        creatureArrayUndo = new List<Creature>(creatureArray);
        Debug.Log(creatureArrayUndo.Count);
        Debug.Log(creatureArray.Count);
    }


    // Active creature controller
    private void PreviousCreature()
	{
		playingCreaturePosition--;
        if(playingCreaturePosition < 0)
        {
            playingCreaturePosition = creatureArray.Count-1;
        }
        PrintCreatures();
	}
    private void NextCreature()
    {
        playingCreaturePosition++;
        if(playingCreaturePosition >= creatureArray.Count)
        {
            playingCreaturePosition = 0;
        }
        PrintCreatures();
    }

    // Button New listener
    private void NewCreatureListener()
    {
        UndoPrep();
        NewCreature(inputName.text, int.Parse(inputInit.text), int.Parse(inputHp.text));
    }
    // Panel Write Liste control
    private void OpenPanelWrite()
    {
        panelWriteListe.SetActive(true);
        ioControl.PopulateLoadList();
    }
    public void ClosePanelWrite()
    {
        panelWriteListe.SetActive(false);
    }

    // Panel Load Liste control
    private void OpenPanelLoad()
    {
        panelLoadListe.SetActive(true);
        ioControl.PopulateLoadList();
    }
    public void ClosePanelLoad()
    {
        panelLoadListe.SetActive(false);
    }

    // Panel Add Creature control
    private void OpenPanelAdd()
    {
        panelAddCreatures.SetActive(true);
    }
    private void ClosePanelAdd()
    {
        panelAddCreatures.SetActive(false);
    }

    // Panel action control
    private void OpenPanelAction()
    {
        panelAction.SetActive(true);
    }
    private void ClosePanelAction()
    {
        panelAction.SetActive(false);
    }

    // Applies actions to creature
    private void ConfirmAction()
    {
        UndoPrep();
        if (creatureArray.Count != 0)
        {
            int damageValue = int.Parse(inputDamage.text);
            int targetPosition = dropdown1.value;

            string logText = dropdown1.options[targetPosition].text + " has been healed by " + inputDamage.text + "!\n";
      
            if (!buttonDamage.IsInteractable())
            {
                logText = dropdown1.options[targetPosition].text + " has taken " + inputDamage.text + " damage!\n";
                damageValue = damageValue * -1;

            }
            logController.AddtoLog(logText);

            creatureArray[targetPosition].setHp(damageValue);
            if (creatureArray[targetPosition].getHp() <= 0) {
                logController.AddtoLog(dropdown1.options[targetPosition].text + " has died!\n");
                for (int i = targetPosition; i < creatureArray.Count - 1; i++)
                {
                    creatureArray[i] = creatureArray[i + 1];
                }

                creatureArray.RemoveAt(creatureArray.Count - 1);
            }
            PrintCreatures();
        }
        
        panelAction.SetActive(false);
    }

    // Controls UI of action panel
    private void DamageSelected()
    {
        buttonDamage.interactable = false;
        buttonHealing.interactable = true;
    }
    private void HealingSelected()
    {
        buttonDamage.interactable = true;
        buttonHealing.interactable = false;
    }

    // New creature controller
    public void NewCreature(string name, int init, int hp)
    {
        Creature creature = new Creature(name, init, hp);
        SortCreature(creature);
       
    }

    private void SortCreature(Creature creature)
    {
        if (creatureArray.Count == 0)
        {
            creatureArray.Add(creature);
            highestInit = creature.getInit();
            lowestInit = creature.getInit();

        }
        else if (creature.getInit() >= highestInit)
        {
            AddTop(creature);
            highestInit = creature.getInit();
        }
        else if (creature.getInit() <= lowestInit)
        {
            creatureArray.Add(creature);
            lowestInit = creature.getInit();
        }
        else
        {
            AddMiddle(creature);
        }
        PrintCreatures();
        panelAddCreatures.SetActive(false);
    }

    //Sorting functions
    private void AddTop(Creature creature)
    {
        creatureArray.Add(creatureArray[creatureArray.Count - 1]);
        for (int i = creatureArray.Count - 1; i >= 2; i--)
        {
            creatureArray[i - 1] = creatureArray[i - 2];
        }
        creatureArray[0] = creature;
    }
    private void AddMiddle(Creature creature)
    {
        bool positionFound = false;
        int aimedPosition = 0;
        int positionSearch = 1;
        while (!positionFound)
        {
            if (creature.getInit() >= creatureArray[positionSearch].getInit())
            {
                positionFound = true;
                aimedPosition = positionSearch;
            }
            else
            {
                positionSearch++;
            }
        }
        creatureArray.Add(creatureArray[creatureArray.Count - 1]);

        for (int i = creatureArray.Count - 1; i >= 2 + aimedPosition; i--)
        {
            creatureArray[i - 1] = creatureArray[i - 2];
        }
        creatureArray[aimedPosition] = creature;
    }
    
    // Controls the printing of the list
    private void PrintCreatures()
    {
        dropdown1.ClearOptions();
        ClearText(false);
        for(int i = 0; i < creatureArray.Count; i++)
        {
			creatureArray[i].resetPrintText();
            if (playingCreaturePosition == i)
            {
                creatureArray[playingCreaturePosition].setPrintText("*" + creatureArray[playingCreaturePosition].getPrintText() + "*");
            }
            textArray[i].text = creatureArray[i].getPrintText();
            dropdown1.options.Add(new Dropdown.OptionData(creatureArray[i].getName()));
        }
        dropdown1.RefreshShownValue();
    }

    // Control of the clear 
    private void ButtonClear()
    {
        UndoPrep();
        ClearText(true);
    }
    public void ClearText(bool totalClear)
    {
        dropdown1.ClearOptions();
        foreach ( Text text in textArray)
        {
            text.text = "";
        }
        if (totalClear)
        {
            creatureArray.Clear();
        }
    }


 
}





