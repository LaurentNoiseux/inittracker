﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PushStatBlock : MonoBehaviour {

    public GameObject statBlock;

    public GameObject nameGoesHere;
    public GameObject titleGoesHere;
    public GameObject tGoesHere;
    public GameObject dGoesHere; 
    public GameObject aGoesHere;

    public string nyme;
    public string title;
    public string t;
    public string d;
    public string a;

    private Adventurer aventurier;

    

    public void PushBlock() {

        nameGoesHere.GetComponent<Text>().text = nyme;
        
        titleGoesHere.GetComponent<Text>().text = title;

        tGoesHere.GetComponent<Text>().text = t;
        dGoesHere.GetComponent<Text>().text = d;
        aGoesHere.GetComponent<Text>().text = a;

        statBlock.SetActive(true);
        
        
    }

   
    //public void Startup(string Name, string Title, int Kills, int Downed, int Assist)
    //{
    //    aventurier = new Adventurer();
            
    //}
}
