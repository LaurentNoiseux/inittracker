﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature {

    private string name, printText;
    private int init;
    private int currentHp, maxHp;
    

    public Creature(string tempCreatureName, int tempInit, int tempHp)
    {
        name = tempCreatureName;
        init = tempInit;
        currentHp = tempHp;
        maxHp = tempHp;
        printText = name + " Hp: " + currentHp;
    }
    public void setHp(int value)
    {
        if (value + currentHp > maxHp)
        {
            currentHp = maxHp;
        }
        else
        {
            currentHp = currentHp + value;
        }
    }
    public void setPrintText(string newPrint)
    {
        printText = newPrint;
    }


    public string getName()
    {
        return name;
    }

    public int getInit()
    {
        return init;
    }
    public int getHp()
    {
        return currentHp;
    }
  
    public string getPrintText()
    {
        return printText;
    }

    public string getWriteText()
    {
        return name + " " + init + " " + currentHp;
    }
    public void resetPrintText()
    {
        printText = name + " Hp: " + currentHp;
    }
}
