﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Greenhorn", menuName = "Green Horn", order = 50)]

//Made a class for adventurer's stats. Name/titles are obviously strings. KDA are integers. Death is a bool.
public class Adventurer : Creature
{

    public string classe;
    public int kills;
    public int downed;
    public int assist;
    public bool dead;


    /// <summary>
    /// Constructeur de nouveau aventurier
    /// </summary>
    /// <param name="tempName"></param>
    /// <param name="tempClass"></param>
    public Adventurer(string tempName, string tempClass) : base(tempName, 0, 0) {
        classe = tempClass;
        kills = 0;
        downed = 0;
        assist = 0;
        dead = false;
    }

    /// <summary>
    /// Constructeur pour un aventurier déjà existant
    /// </summary>
    /// <param name="tempName"></param>
    /// <param name="tempClass"></param>
    /// <param name="tempKill"></param>
    /// <param name="tempDowned"></param>
    /// <param name="tempAssist"></param>
    public Adventurer(string tempName, string tempClass, int tempKill, int tempDowned, int tempAssist) : base(tempName, 0, 0)
    {
        classe = tempClass;
        kills = tempKill;
        downed = tempDowned;
        assist = tempAssist;
        dead = false;
    }


    public int getKills()
    {
        return kills;
    }
    public int getDowned()
    {
        return downed;
    }

    public int getAssists()
    {
        return assist;
    }
    public bool isDead()
    {
        return dead;
    }

    //private void Awake()
    //{
    //    GameObject tab = GameObject.Find("AdventurerTab");
    //}

}



