﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class IOScript : MonoBehaviour {

    public Controller gameController;
    public Dropdown dropdownLoadList;
    public Button buttonLoad, buttonWrite;
    public InputField inputPath;
    public List<string> pathList;


	// Use this for initialization
	void Start () {
        dropdownLoadList.ClearOptions();
        buttonLoad.onClick.AddListener(Read);
        buttonWrite.onClick.AddListener(WriteEvent);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void WriteEvent()
    {
        Write(gameController.creatureArray);
    }
    public void Read()
    {
        gameController.ClearText(true);
        string path = "Assets/ListData/" + dropdownLoadList.options[dropdownLoadList.value].text;

        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream)
        {
            String[] line = reader.ReadLine().Split(); 
            gameController.NewCreature(line[0], int.Parse(line[1]), int.Parse(line[2]));
        }
        
        reader.Close();
        gameController.ClosePanelLoad();
    }

    public void Write(List<Creature> creatureArray)
    {
        string wantedPath = inputPath.text + ".txt";
        bool pathExists = false;
        int i = 0;
        foreach (string path in pathList)
        {
            if ((path.Remove(0, 16)).Equals(wantedPath))
            {
                pathExists = true;
                Debug.Log("Path Exists");
            }
        }
        Debug.Log("Out of the loop");
        if (!pathExists && !wantedPath.Equals(".txt"))
        {
            StreamWriter writer = new StreamWriter("Assets/ListData/" + wantedPath, true);
            foreach(Creature creature in creatureArray)
            {
                writer.WriteLine(creature.getWriteText());
            }
            writer.Close();
        }
        else
        {
            Debug.Log("Path error");
        }
        gameController.ClosePanelWrite();
    }
    public void PopulateLoadList()
    {
        dropdownLoadList.ClearOptions();
        foreach (string file in System.IO.Directory.GetFiles("Assets/ListData"))
        {
            if(file.EndsWith(".txt"))
            {
                pathList.Add(file);
            }
        }
        foreach(string path in pathList)
        {
            dropdownLoadList.options.Add(new Dropdown.OptionData(path.Remove(0, 16)));
        }
        
        dropdownLoadList.RefreshShownValue();
    }
}
