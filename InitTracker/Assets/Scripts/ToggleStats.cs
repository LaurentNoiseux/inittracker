﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleStats : MonoBehaviour
{
    public void Toggle()
    {
        if (gameObject.activeSelf == false)
        {
            this.gameObject.SetActive(true);
        }

        else if (gameObject.activeSelf == true)
        {
            this.gameObject.SetActive(false);
        }
    }
}
